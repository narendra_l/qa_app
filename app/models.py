# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth import get_user_model


class CreatedMixin(models.Model):
	"""Abstract class to hold the created mixin"""
	created_by = models.ForeignKey(get_user_model())
	created_on = models.DateTimeField(auto_now_add=True)

	class Meta:
		abstract = True


class Question(CreatedMixin):
	"""Models to hold questions"""

	question = models.TextField()
	help = models.TextField()
	score = models.IntegerField(default=0)

	def __str__(self):
		return "{} - {}".format(self.id, self.question)


class Answers(models.Model):
	"""Models to hold answers"""

	answer = models.CharField(max_length=200)
	is_correct = models.BooleanField(default=False)
	question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')

	def __str__(self):
		return "{} - {}".format(self.id, self.answer)


class Test(CreatedMixin):
	"""Model to hold tests"""

	description = models.TextField()
	questions = models.ManyToManyField(Question)

	def __str__(self):
		return "{} - {}".format(self.id, self.description)


class TestAttempts(models.Model):
	"""Model to hold test attempts"""

	test = models.ForeignKey(Test, on_delete=models.CASCADE)
	score = models.IntegerField(default=0)
	attempted_by = models.ForeignKey(get_user_model())
	attempted_on = models.DateTimeField(auto_now_add=True)

	@classmethod
	def header(cls):
		return [
			'ID',
			'Username',
			'Score',
			'Attempted date'
		]

	def get_data(self):
		return [
			self.id,
			self.attempted_by.username,
			self.score,
			str(self.attempted_on),
		]


	def __str__(self):
		return "{} - {}% - {} - {}".format(self.id, self.score, self.attempted_by, self.test)

