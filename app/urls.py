from rest_framework import routers
from app import views
from django.conf.urls import url, include

router = routers.DefaultRouter()
router.register(r"test", views.TestView)

urlpatterns = [
    url("api/", include(router.urls)),
    url("rest_api/", include("rest_framework.urls", namespace="rest_framework")),
    url(
        "test_attempt/(?P<test_id>\d+)/$",
        views.TestAttempt.as_view(),
        name="test_attempt",
    ),
    url("test_result/$", views.ResultsVerify.as_view(), name="test_result"),
    url("download", views.download, name="download"),
    url("", views.Home.as_view(), name="home"),
]
