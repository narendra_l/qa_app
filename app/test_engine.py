from app.models import Test

PASSED = "PASSED"
FAILED = "FAILED"


class TestEngine(object):
    """
    Test engine to calculate the scores
    """

    def __init__(self, test_id, test_data):
        self.test_obj = Test.objects.get(pk=test_id)
        self.test_data = {x["question"]: x for x in test_data}

    def calculate_score(self):
        """
        Calculates the score
        :return:
        """
        scoring = {}
        result = []

        for quest in self.test_obj.questions.all():

            test_quest = self.test_data[quest.question]
            scoring[quest.question] = [
                self.match(ans, test_quest["answers"]) for ans in quest.answers.all()
            ]

        for question, matches in scoring.items():
            result.append(
                {"question": question, "result": PASSED if all(matches) else FAILED}
            )

        scoring_temp = [d["result"] for d in result]
        result_break_down = {
            "result_graph": [
                [PASSED, scoring_temp.count(PASSED)],
                [FAILED, scoring_temp.count(FAILED)],
            ],
            "result_breakdown": {
                PASSED: scoring_temp.count(PASSED),
                FAILED: scoring_temp.count(FAILED),
            },
            "score": (scoring_temp.count(PASSED) / (len(scoring_temp) * 1.0)) * 100,
        }

        return result, result_break_down

    def match(self, ref_answer, test_answers):
        """
        Matches the reference answers to the answerd
        :param ref_answer:
        :param test_answers:
        :return:
        """

        for test_answer in test_answers:
            if (
                test_answer["ans"] == ref_answer.answer
                and test_answer["active"] == ref_answer.is_correct
            ):
                return True
        return False
