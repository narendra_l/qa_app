from rest_framework.authentication import SessionAuthentication

class CsrfExemptSessionAuthentication(SessionAuthentication):
    """To disable CSRF for rest framework"""

    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening
