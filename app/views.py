# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
import django_excel as excel

from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import viewsets
from app.serializer import TestSerializer
from rest_framework.authentication import BasicAuthentication
from app.middlewares import CsrfExemptSessionAuthentication
from django.contrib.auth.mixins import LoginRequiredMixin

from app.models import Test, TestAttempts
from django.views import View
from app.test_engine import TestEngine
from django.contrib.auth.decorators import user_passes_test



class Home(LoginRequiredMixin, View):
    """Home view"""

    login_url = '/login/'
    redirect_field_name = 'redirect_to'

    def get(self, request, *args, **kwargs):
        """Handles get http method"""
        return render(request, "index.html")


class TestAttempt(LoginRequiredMixin, View):
    """REST API to serve Test attempt"""

    login_url = '/login/'
    redirect_field_name = 'redirect_to'

    def get(self, request, test_id, *args, **kwargs):
        """Handles get http method"""
        print(test_id, '>>>>')
        return render(request, "test_attempt.html", {'test_id': test_id})


class TestView(viewsets.ModelViewSet):
    """REST API to provide tests"""
    queryset = Test.objects.all()
    serializer_class = TestSerializer


class ResultsVerify(APIView):
    """API to verify the results"""
    authentication_classes = (
        CsrfExemptSessionAuthentication,
        BasicAuthentication,
    )

    def post(self, request):
        """Handles post call"""
        print(request.POST)
        test_id = request.data['test_id']
        te = TestEngine(test_id, test_data=json.loads(request.data['qa']))
        result, result_bd = te.calculate_score()

        test_attempt = TestAttempts(
            test_id=test_id,
            score=result_bd['score'],
            attempted_by=request.user
        )
        test_attempt.save()

        return Response({'result': result, 'result_bd': result_bd})


@user_passes_test(lambda u: u.is_superuser)
def download(request):
    """Get method to download test attempts as XL sheets"""
    data = [
        TestAttempts.header(),
    ]
    data.extend([t.get_data() for t in TestAttempts.objects.order_by('-attempted_by').all()])
    sheet = excel.pe.Sheet(data)
    return excel.make_response(sheet, "csv")