from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.test import TestCase
from django.contrib.auth import get_user_model

from app.models import Test as QATest


User = get_user_model()


class HomeTests(TestCase):
    """
    View to test the Report Components
    """
    url = reverse("app:home")


    def setUp(self):
        """
        Setup testcase
        :return:
        """
        User.objects.create_user(username='testuser', password='testuser')
        User.objects.create_superuser('admin', 'myemail@test.com', password='admin')

    def test_home_redirect(self):
        """
        Test the home page redirection when no login case
        :return:
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

    def test_home_access(self):
        """
        Test home page access after login
        :return:
        """
        self.client.login(username='testuser', password='testuser')
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_home_access_admin(self):
        """
        Test home page access after login
        :return:
        """
        self.client.login(username='admin', password='admin')
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Download Tests XL')

    #  Similar unit tests can be written to make sure the code working perfectly



class QATestTests(APITestCase):
    """
    Test the QATestTests
    """

    def setUp(self):
        test_user = User.objects.create_user(username='testuser', password='testuser')
        self.test = QATest(description="Python test", created_by=test_user)
        self.test.save()

    def test_test_data(self):
        """
        Test to verify if the response is as expected
        :return:
        """
        response = self.client.get('/api/test/{}/'.format(self.test.id))
        self.assertContains(response, 'Python test')

    #  Similar unit tests can be written to make sure the code working perfectly
