# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from django.contrib import admin
from app import models as app_models


class BaseAdmin(admin.ModelAdmin):

    exclude = ['created_by', ]

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        super(BaseAdmin, self).save_model(request, obj, form, change)


class AnswersAdminInline(admin.TabularInline):
    model = app_models.Answers


@admin.register(app_models.Test)
class TestAdmin(BaseAdmin):
    """
    Admin class for Test
    """


@admin.register(app_models.Question)
class QuestionAdmin(BaseAdmin):
    """
    Admin class for Question
    """
    inlines = (AnswersAdminInline,)
    exclude = ['created_by', ]


@admin.register(app_models.Answers)
class AnswerAdmin(BaseAdmin):
    """
    Admin class for Answer
    """
    exclude = ['created_by', ]


@admin.register(app_models.TestAttempts)
class TestAttemptsAdmin(BaseAdmin):
    """
    Admin class for TestAttemps
    """
