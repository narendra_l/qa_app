
Supported features:
----------------------
1. So are supposed to create a web application where the admin user can create a list of Multiple Choice Questions.
2. Different people can separately answer those multiple choice questions on which they will be scored.
3. The score of each test should be displayed to the test taker at the end of the test in a graph
4. The admin should be able to see the results of all the tests in the excel sheet.
5. The records in the excel sheet should be ordered in a way such that the latest test taken is on the top.


Steps to deploy
____________________
1. Create virtual env
    virtualenv ~/virtual_envs/innoscripta_new -p python2.7
    source  ~/virtual_envs/innoscripta_new/bin/activate.fish
    
2. Install dependencies
    pip install -r requirements.txt
3. Migrate the database
    python manage.py migrate
4. Create a super user
    python manage.py createsuperuser
5. Run the server
    python manage.py runserver 0.0.0.0:8080
6. Login to admin and add tests
    http://localhost:8080/admin/

Backend:
-------------------------------------------------
Python 2.7 
Django

Frontend:
--------------------------------------------------
Bootstrap
Vuejs


Deployed version of task is available at
-------------------------------------------------
http://ec2-18-216-172-4.us-east-2.compute.amazonaws.com:8099/


Demo video available at
------------------------
https://vimeo.com/327005647


Users:
-----------------
Admin user: naren
Password: passpass

Normal user: nami
password: naminami


Running unittest:
-------------------
python manage.py test                                                                                                                              Thu Mar 28 13:54:44 2019
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
....
----------------------------------------------------------------------
Ran 4 tests in 0.389s

OK
Destroying test database for alias 'default'...



Database hierarchy
------------------------
1. Each Tests can have many questions
2. Each Questions can have many answers
3. Any user can have many test attempts

